from django import forms
from dynamic_forms import DynamicField, DynamicFormMixin

from .models import Course, Module


class UniversityForm(DynamicFormMixin, forms.Form):

    def module_choices(form):
        course = form['course'].value()
        return Module.objects.filter(course=course)

    def initial_module(form):
        course = form['course'].value()
        return Module.objects.filter(course=course).first()        


    course = forms.ModelChoiceField(
        queryset=Course.objects.all(),
        initial=None
    )
 
    modules = DynamicField(
        forms.ModelChoiceField,
        queryset=module_choices,
        initial=None,
        include=lambda form: form["course"].value() != None
    )


class MakeAndModelForm(DynamicFormMixin, forms.Form):
    MAKE_CHOICES = [
        ("audi", "Audi"),
        ("toyota", "Toyota"),
        ("bmw", "BMW"),
    ]

    MODEL_CHOICES = {
        "audi": [
            ("a1", "A1"),
            ("a3", "A3"), 
            ("a6", "A6"),
        ],
        "toyota": [
            ("landcruiser", "Landcruiser"),
            ("tacoma", "Tacoma"),
            ("yaris", "Yaris"),
        ],
        "bmw": [
            ("325i", "325i"),
            ("325ix", "325ix"),
            ("x5", "X5"),
        ],
    }

    make = forms.ChoiceField(
        choices=MAKE_CHOICES,
        initial="audi",
    )
    model = DynamicField(
        forms.ChoiceField,
        choices=lambda form: form.MODEL_CHOICES[form["make"].value()],
        include=lambda form: form["make"].value() == "audi",
    )