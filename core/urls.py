from django.urls import path

from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("htmx_course/", views.htmx_courses, name="htmx_courses"),
    path('django-forms-dynamic-course/', views.courses, name='dynamic_courses'),
    path('modules/', views.modules, name='modules'),
    path("htmx_modules/", views.htmx_modules, name="htmx_modules"),
    path("submit_university_form_htmx/", views.submit_university_form_htmx, name="submit_university_form_htmx/"),
    path("htmx-form/", views.htmx_form),
    path("htmx-form/models/", views.htmx_models),
]