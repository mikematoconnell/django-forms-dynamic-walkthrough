from django.http import HttpResponse
from django.shortcuts import render

from core.models import Course, Module

from .forms import MakeAndModelForm, UniversityForm


# Create your views here.
def home(request):
    return render(request, "base.html")

def courses(request):
    form = UniversityForm()
    context = {'form': form}
    
    return render(request, 'university.html', context)

def modules(request):
    form = UniversityForm(request.GET)

    return HttpResponse(form['modules'])

def htmx_courses(request):
    courses = Course.objects.all()
    context = {"courses": courses}

    return render(request, "university_htmx.html", context)

def htmx_modules(request):
    course = request.GET.get("course")
    if course == "null":
        context = {}

        return render(request, "partials/modules.html", context)
    
    modules = Module.objects.filter(course=course)
    context = {"modules": modules}

    return render(request, "partials/modules.html", context)

def submit_university_form_htmx(request):
    print(request)
    data = request.POST
    print(data)

    courses = Course.objects.all()
    context = {"courses": courses}

    return render(request, "university_htmx.html", context)

def htmx_form(request):
    form = MakeAndModelForm()
    
    return render(request, "make_model.html", {"form": form})


def htmx_models(request):
    form = MakeAndModelForm(request.GET)

    return HttpResponse(form["model"])